/**
 * Express router for handling analytics routes.
 * @module routes/analyticsRoutes
 */

const path = require('path')

const express = require('express');
const router = express.Router();
const logger = require('../controllers/log.controller');

// routes.js
const AnalyticsController = require('../controllers/analyticsController');

/**
 * Route for serving the analytics page.
 * @name GET /
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
//router.get('/', function(req, res) {
//    res.sendFile(path.resolve(__dirname, '../../public/analytics.html'));
//});
router.get('/', (req, res) => {
    // Check if the user is logged in by checking the session
    if (req.session.user) {
        res.sendFile(path.resolve(__dirname, '../../public/analytics.html'));
    } else {
        res.redirect('../');
        //res.status(401).send('Unauthorized - Please log in'); // or res.redirect('/') example in authRoutes.js line 58 and index.html line 62 ;
    }
});

/**
 * Route for retrieving analytics data.
 * @name POST /get-analytics
 * @function
 * @async
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
router.post('/get-analytics', async (req, res) => {
    if (req.session.user) {
        logger.info(`POST /analytics/get-analytics`);
        const startDate = req.body.startDate;
        const endDate = req.body.endDate;
        logger.info(`startDate: ${startDate}`);
        logger.info(`endDate: ${endDate}`);

        const user_id = req.session.user.id;

        try {
            controller = new AnalyticsController(user_id);
            const expensesOverTime = await controller.getExpensesOverTime(startDate, endDate);
            const expensesByCategory = await controller.getExpenseCategoriesBreakdown(startDate, endDate);
            const averageSpending = await controller.getAverageSpending(startDate, endDate);

            /**
             * Object containing analytics functions.
             * @typedef {Object} Analytics
             * @property {Function} expensesOverTime - Function to calculate expenses over time.
             * @property {Function} expensesByCategory - Function to calculate expenses by category.
             * @property {Function} averageSpending - Function to calculate average spending.
             */
            const analytics = {
                expensesOverTime,
                expensesByCategory,
                averageSpending
            };
            logger.info(`Analytics data extracted`);
            res.json({ data: analytics });
        } catch (error) {
            console.error('Error occurred:', error);
            res.redirect('../');
        }
    } else {
        res.status(401).send('Unauthorized - Please log in'); // or res.redirect('/') example in authRoutes.js line 58 and index.html line 62 ;
    }
});

module.exports = router;
