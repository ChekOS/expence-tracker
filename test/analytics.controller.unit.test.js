// test/analyticsController.test.js

const chai = require('chai');
const sinon = require('sinon');
const AnalyticsController = require('../src/controllers/analyticsController');
const AnalyticsModel = require('../src/models/analyticsModel');

const expect = chai.expect;

describe('AnalyticsController', () => {
  afterEach(() => {
    sinon.restore();
  });

  it('should get expenses over time', async () => {
    const expensesStub = sinon.stub(AnalyticsModel, 'queryExpensesOverTime').resolves([]);
    controller = new AnalyticsController(1);
    const expenses = await controller.getExpenseCategoriesBreakdown('2022-01-01', '2022-01-31');
    expect(expenses).to.be.an('array');
  });

  it('should get expense categories breakdown', async () => {
    const expensesStub = sinon.stub(AnalyticsModel, 'queryExpenseCategoriesBreakdown').resolves([]);
    controller = new AnalyticsController(1);
    const expenses = await controller.getExpenseCategoriesBreakdown('2022-01-01', '2022-01-31');
    expect(expensesStub.calledOnce).to.be.true;
    expect(expenses).to.be.an('array');
  });

  it('should get average spending', async () => {
    const expensesStub = sinon.stub(AnalyticsModel, 'queryAverageSpending').resolves(1000);
    controller = new AnalyticsController(1);
    const averageSpending = await controller.getAverageSpending('2022-01-01', '2022-01-31');
    expect(expensesStub.calledOnce).to.be.true;
    expect(averageSpending).to.be.an('object');
    expect(averageSpending).to.have.property('totalDays');
    expect(averageSpending).to.have.property('totalAmount');
    expect(averageSpending).to.have.property('averageSpending');
  });
});