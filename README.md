![Logo](https://gitlab.com/ChekOS/expence-tracker/-/raw/dev/assets/logo.png)

# :page_facing_up: Table of Contents 

- [:page\_facing\_up: Table of Contents](#page_facing_up-table-of-contents)
- [:rocket: Track your wallet!](#rocket-track-your-wallet)
  - [:star: Features](#star-features)
  - [:toolbox: Tech Stack](#toolbox-tech-stack)
  - [:file\_folder: Project structure](#file_folder-project-structure)
  - [:computer: Local deployment](#computer-local-deployment)
- [⚖️ License](#️-license)
# :rocket: Track your wallet!

Welcome to the [ExpenseTracker Web Application](http://172.30.134.119/expense-tracker/)! Easily track expenses, set budgets, and gain insights to achieve your financial goals. Simplify your life and take control of your money today!

![Version](https://img.shields.io/badge/Version-1.0-blue.svg)
[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/) \
![Python](https://img.shields.io/badge/Node.js-20.10.0-blue)
![Flask](https://img.shields.io/badge/HTML-5-yellow)
![TensorFlow](https://img.shields.io/badge/Nginx-1.25.3-orange)
![Microsoft Azure](https://img.shields.io/badge/Microsoft%20Azure-Cloud-0089D6)
![BERT](https://img.shields.io/badge/Backend-ff6600)
![Docker](https://img.shields.io/badge/Frontend-blue)
![REST](https://img.shields.io/badge/REST-API-5b68d6)


![Demo](https://gitlab.com/ChekOS/expence-tracker/-/raw/dev/assets/preview.png)


## :star: Features
- **Table of Expenses**: View and manage your expenses at a glance with a clear and organized table. Easily track transactions, dates, and categories for a comprehensive overview of your spending habits.

- **Create Expense**: Effortlessly add new expenses on the go. Input transaction details, assign categories, and include relevant information to keep your records accurate and up-to-date.

- **Analytics with Diagram and Chart**: Gain valuable insights into your financial patterns through dynamic diagrams and charts. Visualize your spending trends, budget adherence, and savings goals, making it easier to make informed decisions and optimize your financial strategy.

- **Authorization and Security**: Securely log in to your account with our robust authorization system, ensuring your financial data is protected and accessible only to you.

## :toolbox: Tech Stack

- **Framework**: NodeJS
- **Deployment**: CI/CD, Microsoft Azure
- **Frontend**: HTML, CSS, JavaScript, Bootstrap
- **Version Control**: Git, GitLab, GitHub
- **Testing**: REST client, Unit tests, Integration tests

## :file_folder: Project structure
```
|   .env                    <- Environment variables
|   .env.example            <- example .env structure
|   .gitignore              <- Git configuration for ignored files
|   app.js                  <- Script to start the app
|   init_db.sql             <- SQL script for initializing the database
|   package-lock.json       <- Lock file for npm package versions
|   package.json            <- Project configuration file
|   README.md               <- Project documentation
|   rest.http               <- HTTP requests for testing the RESTful API
|   
+---assets
|       logo.png            <- Logo image for branding
|       
+---config
|       db.js               <- Database connection configuration
|       
|
+---public
|       analytics.html      <- HTML file for analytics view
|       expense_form.html   <- HTML file for expense form view
|       expense_list.html   <- HTML file for expense list view
|       index.html          <- Main HTML file
|       main.css            <- Stylesheet for HTML views
|       
+---src
|   +---controllers
|   |       analyticsController.js          <- Controller for analytics logic
|   |       authenticationController.js     <- Controller for authentication logic
|   |       categoryController.js           <- Controller for category logic
|   |       expenseController.js            <- Controller for expense logic
|   |       log.controller.js               <- Controller for logging logic
|   |       
|   +---middlewares
|   |       entry.middleware.js             <- Middleware for entry point
|   |       json_secure.middleware.js       <- Middleware for JSON security
|   |       
|   +---models
|   |       analyticsModel.js               <- Model for analytics data
|   |       authenticationModel.js          <- Model for authentication data
|   |       categoryModel.js                <- Model for category data
|   |       expenseModel.js                 <- Model for expense data
|   |       
|   \---routes
|           analyticsRoutes.js              <- Routes for analytics
|           authenticationRoutes.js         <- Routes for authentication
|           categoryRoutes.js               <- Routes for category
|           expenseRoutes.js                <- Routes for expense
|           
\---test
        analytics.controller.unit.test.js         <- Unit tests for analytics controller
        analytics.integration.test.js             <- Integration tests for analytics
        authentication.controller.unit.test.js    <- Unit tests for authentication controller
        authentication.integration.test.js        <- Integration tests for authentication

```

## :computer: Local deployment

1. Clone the project

```bash
  git clone https://gitlab.com/ChekOS/expence-tracker.git
```

2. Go to the project directory

```bash
  cd expence-tracker
```

3. Install dependencies

```bash
  npm install -y
```

4. Open MySQL client and run init_db.sql

5. Copy .env.example to .env fill in environment variables
```bash
  cp .env.example .env
```
5. Start the server

```bash
  node app.js
```




# ⚖️ License

[MIT](https://gitlab.com/ChekOS/expence-tracker/-/blob/dev/LICENSE)
