const logger = require('../controllers/log.controller');


/**
 * Middleware for JSON security.
 * 
 * @param {import("express").ErrorRequestHandler} err - The error object.
 * @param {import("express").Request} req - The Express request object.
 * @param {import("express").Response} res - The Express response object.
 * @param {import("express").NextFunction} next - The next middleware function.
 */
const json_security = (err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        // Log a warning if there's an error with JSON input
        logger.warn(`Invalid JSON from: ${req.ip}`);
        return res.sendStatus(400); // Send a 400 Bad Request response
    } 
    next(); // Proceed to the next middleware in the chain
};

module.exports = json_security;