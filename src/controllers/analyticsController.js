// expenseController.js
const exp = require('constants');
const logger = require('./log.controller');
const AnalyticsModel = require('../models/analyticsModel');


/**
 * Controller class for handling analytics related operations.
 */
class AnalyticsController {
  /**
   * Creates a new AnalyticsController instance.
   * @param {number} userId - The user ID.
   */
  constructor(userId) {
    this.userId = userId;
  }


  /**
   * Retrieves expenses over a specified time period.
   * @param {Date} startDate - The start date of the time period.
   * @param {Date} endDate - The end date of the time period.
   * @returns {Promise<Array>} - A promise that resolves to an array of expenses with dates.
   */
  async getExpensesOverTime(startDate, endDate) {
    try {
      const expenses = await AnalyticsModel.queryExpensesOverTime(this.userId, startDate, endDate);

      // Generate list of all dates between startDate and endDate
      let currentDate = new Date(startDate);
      endDate = new Date(endDate);
      const allDates = [];
      while (currentDate <= endDate) {
        allDates.push(new Date(currentDate));
        currentDate.setDate(currentDate.getDate() + 1);
      }

      const expensesWithAllDates = allDates.map((date) => {
        const expenseOnDate = expenses[0].find((expense) => {
          const expenseDate = new Date(expense.expenseDate);
          return (
            expenseDate.getFullYear() === date.getFullYear() &&
            expenseDate.getMonth() === date.getMonth() &&
            expenseDate.getDate() === date.getDate()
          );
        });
        if (expenseOnDate) {
          expenseOnDate.expenseDate = new Date(expenseOnDate.expenseDate)
            .toISOString()
            .split("T")[0];
          return expenseOnDate;
        } else {
          return {
            expenseDate: date.toISOString().split("T")[0],
            totalAmount: 0,
          };
        }
      });

      //console.log(expensesWithAllDates);
      logger.info(`Expenses over time extracted`);
      return expensesWithAllDates;
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }


  /**
   * Retrieves the breakdown of expense categories within a specified date range.
   * @param {Date} startDate - The start date of the date range.
   * @param {Date} endDate - The end date of the date range.
   * @returns {Promise<Array>} - A promise that resolves to an array of expense categories breakdown.
   */
  async getExpenseCategoriesBreakdown(startDate, endDate) {
    try {
      const expensesByCategory = await AnalyticsModel.queryExpenseCategoriesBreakdown(this.userId, startDate, endDate);

      //console.log(expensesByCategory);
      logger.info(`Expences by category extracted`);
      return expensesByCategory;
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }


  /**
   * Calculates the average spending within a given date range.
   * 
   * @param {string} startDate - The start date of the range.
   * @param {string} endDate - The end date of the range.
   * @returns {Object} - An object containing the total number of days, total amount spent, and average spending per day.
   */
  async getAverageSpending(startDate, endDate) {
    try {
      // Calculate total days between start and end date
      const start = new Date(startDate);
      const end = new Date(endDate);
      const oneDay = 24 * 60 * 60 * 1000; // Hours * Minutes * Seconds * Milliseconds
      const totalDays = Math.round(Math.abs((end - start) / oneDay)) + 1; // Adding 1 to include both start and end days
      logger.info(`Total days: ${totalDays}`);

      const totalAmount = await AnalyticsModel.queryAverageSpending(this.userId, startDate, endDate);
      logger.info(`Total amount spent: ${totalAmount}`);

      // Calculate average spending per day
      const averageSpending = parseFloat((totalAmount / totalDays).toFixed(2));
      logger.info(`Average spending: ${averageSpending}`);
      return { totalDays, totalAmount, averageSpending };
    } catch (err) {
      console.error('Database query failed:', err);
    }   
  }
}

module.exports = AnalyticsController;
