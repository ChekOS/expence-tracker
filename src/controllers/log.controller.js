const winston = require('winston');

// Create a Winston logger
/**
 * Logger instance for logging messages.
 * @type {winston.Logger}
 */
const logger = winston.createLogger({
  level: 'info', // Log level: info
  format: winston.format.combine(
    winston.format.timestamp(), // Include timestamps in log entries
    winston.format.json() // Log in JSON format
  ),
  transports: [
    new winston.transports.Console(), // Log to the console
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }), // Log error-level messages to 'error.log' file
    new winston.transports.File({ filename: 'logs/combined.log' }) // Log all messages to 'combined.log' file
  ]
});

module.exports = logger;