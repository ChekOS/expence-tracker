// test/analyticsRoutes.test.js
require('dotenv').config();
const request = require('request');
const { expect } = require('chai');
const db = require('../config/db');
const app = require('../app');
const port = process.env.TEST_API_PORT || 3001; 

describe('Authentication API', () => {
  let server;
  before('Start server before running tests', (done) => {
    server = app.listen(port, () => {
      console.log(`Server listening: http://localhost:${port}`);
      done();
    });
  });

  describe('POST /auth/signup', () => {
    it('should return a 409 status if the user already exists', (done) => {
      const existingUser = {
        name: '123',
        login: 'admin',
        password: 'admin',
      };

      request.post(
        {
          url: `http://localhost:${port}/auth/signup`,
          json: existingUser,
        },
        (error, response, body) => {
          expect(response.statusCode).to.equal(409);
          done();
        }
      );
    });
  });

  describe('POST /login', () => {
    it('should log in a user', (done) => {
      const validCredentials = {
        login: 'admin',
        password: 'admin',
      };

      request.post(
        {
          url: `http://localhost:${port}/auth/login`,
          json: validCredentials,
        },
        (error, response, body) => {
          expect(response.statusCode).to.equal(302);
          done();
        }
      );
    });
    it('should return a 409 status and "Wrong credentials" message for invalid credentials', (done) => {
      const invalidCredentials = {
        login: 'nonexistent.user',
        password: 'invalidpassword',
      };

      request.post(
        {
          url: `http://localhost:${port}/auth/login`,
          json: invalidCredentials,
        },
        (error, response, body) => {
          expect(response.statusCode).to.equal(409);
          expect(body).to.equal('Wrong credentials');
          done();
        }
      );
    });
  });
  
  after('Stop server after tests, delete created user', (done) => {
    server.close();
    done();
  });
});