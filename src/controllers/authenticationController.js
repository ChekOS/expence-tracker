// authenticationController.js
const logger = require('./log.controller');
const AuthenticationModel = require('../models/authenticationModel');

/**
 * Controller class for handling authentication-related operations.
 */
class AuthenticationController {

  /**
   * Retrieves all users from the database.
   * @returns {Promise<Array<Object>>} An array of user objects containing id, name, and login.
   */
  static async getUsers() {
    try {
      const [users] = await AuthenticationModel.queryGetUsers();
        const usersArray = users.map(user => ({
            id: user.id,
            name: user.name,
            login: user.login,
        }));
      return usersArray;
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }

  /**
   * Validates the credentials for creating a user.
   * @param {string} name - The name of the user.
   * @param {string} login - The login of the user.
   * @param {string} password - The password of the user.
   * @throws {Error} If the name field is empty.
   * @throws {Error} If the password is less than 8 characters long.
   * @throws {Error} If the password is more than 30 characters long.
   * @throws {Error} If the login field is empty.
   * @returns {boolean} Returns true if the credentials are valid.
   */
  static validateCreatingCredentials(name, login, password,) {
    if (name.length < 1) {
      throw new Error("Name field cannot be empty");
    }
    else if (password.length < 8) {
      throw new Error("Password should be at least 8 characters long");
    }
    else if (password.length > 30) {
      throw new Error("Password should be less than 30 characters");
    }
    else if (login.length < 1) {
      throw new Error("Username field cannot be empty");
    }
    return true;
  }

  /**
   * Validates user login.
   * @param {string} userLogin - The user login to be validated.
   * @returns {Promise<Object|null>} - The validated user login or null if validation fails.
   */
  static async validateUserLogin(userLogin) {
    try {
      const [login] = await AuthenticationModel.queryValidateUserLogin(userLogin);
      return login;
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }
  
  /**
   * Creates a new user.
   * @param {Object} userInfo - The user information.
   * @param {string} userInfo.login - The user's login.
   * @param {string} userInfo.password - The user's password.
   * @returns {Promise<void>} - A promise that resolves when the user is created.
   */
  static async createUser(userInfo) {
    try {
      await AuthenticationModel.queryCreateUser(userInfo);
      logger.info(`user ${userInfo.login} created`);
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }
}

module.exports = AuthenticationController;
