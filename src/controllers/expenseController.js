// expenseController.js

const ExpenseModel = require('../models/expenseModel');
const CategoryModel = require('../models/categoryModel');

/**
 * ExpenseController class represents a controller for managing expenses.
 */
class ExpenseController {
  /**
   * Creates a new AnalyticsController instance.
   * @param {number} userId - The user ID.
   */
  constructor(userId) {
    this.userId = userId;
  }

  /**
   * Retrieves expenses for the current user.
   * @returns {Promise<Array>} An array of expenses.
   */
  async getExpenses() {
    try {
      const expenses = await ExpenseModel.queryExpenses(this.userId);
      console.log(expenses);
      return expenses;
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }

  /**
   * Creates an expense with the provided expense data.
   * @param {Object} expenseData - The data for the expense.
   * @param {string} expenseData.description - The description of the expense.
   * @param {number} expenseData.amount - The amount of the expense.
   * @param {string} expenseData.category - The category of the expense.
   * @returns {Promise<boolean>} - A promise that resolves to true if the expense is created successfully, false otherwise.
   * @throws {Error} - If there is an error creating the expense.
   */
  async createExpense(expenseData) {
    // Validate form values
    if (!expenseData.description || !expenseData.amount) {
      return false;
    }

    // Convert amount to a number
    const amount = Number(expenseData.amount);
    if (isNaN(amount)) {
      return false;
    }

    // Create Date object
    const date = new Date();

    // Check if category exists
    try {
      const categories = await CategoryModel.queryCategoriesByName(expenseData.category);
      let categoryId = null;
      if (categories.length > 0) {
        categoryId = categories[0].id;
      }
      if (!categoryId) {
        expenseData.category_id = await CategoryModel.create(expenseData.category);
      } else {
        expenseData.category_id = categoryId;
      }
    } catch (err) {
      console.error('Failed to create expense:', err);
      throw err;
    }

    // Create the expense
    try {
      const expense = await ExpenseModel.create({
        amount,
        description: expenseData.description,
        category_id: expenseData.category_id,
        date,
        user_id: this.userId,
      });
      return true;
    } catch (err) {
      console.error('Failed to create expense:', err);
      throw err;
    }
  }
}

module.exports = ExpenseController;
