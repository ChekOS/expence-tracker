const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const entryMW = require('./src/middlewares/entry.middleware');
const app = express();
require('dotenv').config();
const port = process.env.API_PORT;
const host = process.env.API_HOST;

// Configure middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(session({
    secret: 'testkey',
    resave: false,
    saveUninitialized: false,
    cookie: { sameSite: 'strict' },
}));
app.use(express.static(__dirname + '/public'));
//console.log(__dirname + '/public');
// app.use(express.static('public'));
app.use(entryMW);

// Configure routes
const expenseRoutes = require('./src/routes/expenseRoutes');
const categoryRoutes = require('./src/routes/categoryRoutes');
const analyticsRoutes = require('./src/routes/analyticsRoutes');
const authenticationRoutes = require('./src/routes/authenticationRoutes');
app.use('/expenses', expenseRoutes);
app.use('/categories', categoryRoutes);
app.use('/analytics', analyticsRoutes);
app.use('/auth', authenticationRoutes);

// Start the server
app.listen(port, host, () => {
    console.log(`Server is running on port ${port}`);
});

module.exports = app;