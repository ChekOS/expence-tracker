/**
 * This file contains the routes for managing expenses.
 * @module expenseRoutes
 */

const path = require('path')

const express = require('express');
const router = express.Router();
const logger = require('../controllers/log.controller');

/**
 * ExpenseController handles the logic for managing expenses.
 * @type {import('../controllers/expenseController')}
 */
const ExpenseController = require('../controllers/expenseController');

router.get('/', function(req, res) {
    if (req.session.user) {
        res.sendFile(path.resolve(__dirname, '../../public/expense_list.html'));
    } else {
        res.redirect('../');
    }
});

/**
 * GET request handler for the '/list' route.
 * Retrieves the expenses and sends them as JSON response.
 * @name GET /list
 * @function
 * @async
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 */
router.get('/list', async (req, res) => {
    if (req.session.user) {
        try {
            controller = new ExpenseController(req.session.user.id);
            /**
             * Retrieves the expenses from the controller.
             * @returns {Promise<Array>} The array of expenses.
             */
            const expenses = await controller.getExpenses();
            res.json({ data: expenses });
        } catch (error) {
            console.error('Error occurred:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    } else {
        res.redirect('../');
    }
});

router.get('/create', function(req, res) {
    if (req.session.user) {
        res.sendFile(path.resolve(__dirname, '../../public/expense_form.html'));
    } else {
        res.redirect('../');
    }
});

/**
 * POST request handler for the '/create' route.
 * Creates a new expense and redirects to '/expenses' if successful,
 * otherwise sends the expense_form.html file.
 * @name POST /create
 * @function
 * @async
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 */
router.post('/create', async (req, res) => {
    if (req.session.user) {
        try {
            controller = new ExpenseController(req.session.user.id);
            /**
             * Represents the result of creating an expense.
             * @typedef {boolean} Success
             */
            const success = await controller.createExpense(req.body);

            if (success) {          
                res.redirect('../expenses');
            } else {         
                res.sendFile(path.resolve(__dirname, '../../public/expense_form.html'));
            }
        } catch (error) {
            console.error('Error occurred:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    } else {
        res.redirect('../');
    }
});

module.exports = router;
