/**
 * Express router for authentication routes.
 * @module authenticationRoutes
 */

const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const logger = require('../controllers/log.controller');

const AuthenticationController = require('../controllers/authenticationController');

router.use(express.json());

/**
 * Route handler for the root path.
 * @name GET /
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
router.get('/', (req, res) => {
    res.send("OK");
})

/**
 * Route handler for getting all users.
 * @name GET /users
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
router.get('/users', async (req, res) => {
    if (req.session.user && req.session.user.login === 'admin') {
        const users = await AuthenticationController.getUsers();
        res.json(users);
    }
    else {
        res.status(401).send('Unauthorized - Please log in');
    }
})

/**
 * Route handler for user signup.
 * @name POST /signup
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
router.post('/signup', async (req, res) => {
    try {
        logger.info('POST /auth/signup');
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        const userInfo = {
            name: req.body.name,
            login: req.body.login,
            password: hashedPassword,
        };
        const [existingUser] = await AuthenticationController.validateUserLogin(req.body.login);
        if (existingUser) {
            res.status(409).send('User already exists');
        } else {
            if (AuthenticationController.validateCreatingCredentials(req.body.name, req.body.login, req.body.password)) {
                await AuthenticationController.createUser(userInfo);
                const [user] = await AuthenticationController.validateUserLogin(req.body.login);
                req.session.user = {
                    id: user.id,
                    login: user.login,
                };
                logger.info(`(signup) session for user ${user.login} created`);
                res.redirect('../expenses');  
            }
        }
    } catch (err) {
        logger.error(err);
        res.status(400).json({ error: err.message });
    }
})

/**
 * Route handler for user login.
 * @name POST /login
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
router.post('/login', async (req, res) => {
    const [user] = await AuthenticationController.validateUserLogin(req.body.login);
    if (user == undefined) {
        logger.info(`Wrong credentials for ${req.ip}`);
        return res.status(409).send("Wrong credentials");
    }
    try {
        if (await bcrypt.compare(req.body.password, user.password)) {
            req.session.user = {
                id: user.id,
                login: user.login,
            };
            logger.info(`(login) session for user ${user.login} created`);
            res.redirect('../expenses');
        } else {
            res.status(409).send("Wrong credentials");
        }
    } catch (err) {
        logger.error(err);
        res.status(500).send();
    }
})

/**
 * Route handler for user logout.
 * @name GET /logout
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
router.get('/logout', (req, res) => {
    if (req.session.user) {
        logger.info(`destroying session for user ${req.session.user.login}`);
    }
    req.session.destroy((err) => {
        if (err) {
            console.error(err);
            logger.error(err);
            res.status(500).send();
        } else {
            res.status(200).redirect('../');
        }
    }); 
});

module.exports = router;