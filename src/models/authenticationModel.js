// authenticationModel.js


/**
 * Model for querying a database.
 */
class AuthenticationModel {
/**
 * Configuration object for authenticationModel.
 * @type {Object}
 * @property {Object} db - The database configuration.
 */
  static config = {
    db: require('../../config/db'),
  }

  /**
   * Retrieves all users from the database.
   * @returns {Promise<Array<Object>>} A promise that resolves to an array of user objects.
   */
  static async queryGetUsers() {
    const db = this.config.db;
    const users = await db.query(
      `SELECT * FROM user;`
    );
    return users;
  }

  /**
   * Queries the database to validate a user login.
   * @param {string} userLogin - The login of the user to validate.
   * @returns {Promise<Object>} - A promise that resolves to the login information of the user.
   */
  static async queryValidateUserLogin(userLogin) {
    const db = this.config.db;
    const login = await db.query(
      `SELECT * FROM user WHERE login = ?`, [userLogin]
    );
    return login;
  }
  
  /**
   * Inserts a new user into the database.
   * @param {Object} userInfo - The user information to be inserted.
   * @returns {Promise<void>} - A promise that resolves when the user is successfully inserted.
   */
  static async queryCreateUser(userInfo) {
    const db = this.config.db;
    const insertUserQuery = await db.query(
      `INSERT INTO user SET ?`, [userInfo]
    );
  }
}

module.exports = AuthenticationModel;