// analyticsModel.js


/**
 * Model for querying a database.
 */
class AnalyticsModel {
/**
 * Configuration object for analyticsModel.
 * @type {Object}
 * @property {Object} db - The database configuration.
 */
  static config = {
    db: require('../../config/db'),
  }

/**
 * Queries expenses over a specified time period for a given user.
 * @param {number} userId - The ID of the user.
 * @param {string} startDate - The start date of the time period (YYYY-MM-DD).
 * @param {string} endDate - The end date of the time period (YYYY-MM-DD).
 * @returns {Promise<Array<Object>>} - A promise that resolves to an array of objects representing expenses over time.
 */
  static async queryExpensesOverTime(userId, startDate, endDate) {
    const db = this.config.db;
    const expenses = await db.query(
      `SELECT DATE(date) as expenseDate, ROUND(ABS(SUM(amount)), 2) as totalAmount
      FROM expence_tracker.expense
      WHERE user_id = ? AND amount < 0 AND date BETWEEN ? AND ?
      GROUP BY DATE(date)
      ORDER BY DATE(date)`,
      [userId, startDate, endDate]
    );
    return expenses;
  }

/**
 * Queries the expense categories breakdown for a specific user within a given date range.
 * @param {number} userId - The ID of the user.
 * @param {string} startDate - The start date of the date range.
 * @param {string} endDate - The end date of the date range.
 * @returns {Promise<Object>} - The expenses breakdown by category.
 */
  static async queryExpenseCategoriesBreakdown(userId, startDate, endDate) {
    const db = this.config.db;
    const expensesByCategory = await db.query(
      `SELECT c.name, ROUND(ABS(SUM(amount)), 2) as totalAmount
      FROM expence_tracker.expense
      LEFT JOIN category AS c ON c.id = category_id
      WHERE user_id = ? AND amount < 0 AND date BETWEEN ? AND ?
      GROUP BY c.name`,
      [userId, startDate, endDate]
    );
    return expensesByCategory[0];
  }

/**
 * Queries the average spending for a given user within a specified date range.
 * @param {number} userId - The ID of the user.
 * @param {string} startDate - The start date of the date range.
 * @param {string} endDate - The end date of the date range.
 * @returns {Promise<number>} The total amount of average spending.
 */
  static async queryAverageSpending(userId, startDate, endDate) {
    const db = this.config.db;
    const [result] = await db.query(
      `SELECT ROUND(ABS(SUM(amount)), 2) as totalAmount
      FROM expence_tracker.expense
      WHERE user_id = ? AND amount < 0 AND date BETWEEN ? AND ?`,
      [userId, startDate, endDate]
    );
    const totalAmount = parseFloat((result[0].totalAmount || 0).toFixed(2)); // If there's no expense, default to 0
    return totalAmount;
  }
}

module.exports = AnalyticsModel;