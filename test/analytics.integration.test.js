// test/analyticsRoutes.test.js
require('dotenv').config();
const request = require('request');
const { expect } = require('chai');
const app = require('../app');
const port = process.env.TEST_API_PORT || 3001; 

describe('Analytics API', () => {
  let server;
  before('Start server before running tests', (done) => {
    server = app.listen(port, () => {
      console.log(`Server listening: http://localhost:${port}`);
      done();
    });
  });

  describe('POST /analytics/get-analytics', () => {
    const apiUrl = `http://localhost:${port}/analytics/get-analytics`;

    it.skip('should retrieve analytics data for a valid date range', (done) => {
      request.post(
        {
          url: apiUrl,
          json: { startDate: '2023-11-10', endDate: '2023-12-01' },
        },
        (error, response, body) => {
          expect(response.statusCode).to.equal(200);
          expect(body).to.have.property('data');
          expect(body.data).to.have.property('expensesOverTime');
          expect(body.data).to.have.property('expensesByCategory');
          expect(body.data).to.have.property('averageSpending');
          done();
        }
      );
    });
  });

  after('Stop server after tests', (done) => {
    server.close();
    done();
  });
});